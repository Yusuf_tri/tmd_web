<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>.:Official Website PT TRANSMEDIADATA :.</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Official Website PT TRANS MEDIA DATA" />
	<meta name="keywords" content="Official Website PT TRANS MEDIA DATA" />
	<meta name="author" content="transmediadata.com" />

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="gtco-container">
			<div class="row">
				<div class="col-xs-2">
					<div id="gtco-logo">PT.TRANS MEDIA DATA</div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul>
						<li class="#"><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li class="has-dropdown">
							<a href="#">Services</a>
							<ul class="dropdown">
								<li><a href="#">SUWUN</a></li>
								<li><a href="#">Whitelabel</a></li>
								<li><a href="#">H2H</a></li>
								<li><a href="#">API Interconection </a></li>
							</ul>
						</li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<header id="gtco-header" class="gtco-cover" role="banner" style="background-image:url(images/img_bg_1.jpg);">
		<div class="overlay"></div>
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<p><img src="images/TMD_LOGO.png"></p>
							<p><h1>PT.TRANS MEDIA DATA</h1></p>
							<p><h3>YOUR PAYMENT & ICT SOLUTION</h3></p>
							<p>
								<a href="#" class="btn btn-white btn-outline btn-lg">Download</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div id="gtco-services">
		<div class="gtco-container">
			
			<div class="row animate-box">
				
				<div class="gtco-tabs">
					<ul class="gtco-tab-nav">
						<li class="active"><a href="#" data-tab="1"><span class="icon visible-xs"><i class="icon-command"></i></span><span class="hidden-xs">Company Profile</span></a></li>
						<li><a href="#" data-tab="2"><span class="icon visible-xs"><i class="icon-bar-graph"></i></span><span class="hidden-xs">Payment Divison</span></a></li>
						<li><a href="#" data-tab="3"><span class="icon visible-xs"><i class="icon-bag"></i></span><span class="hidden-xs">Our Produk</span></a></li>
						<li><a href="#" data-tab="4"><span class="icon visible-xs"><i class="icon-box"></i></span><span class="hidden-xs">Our Experinces</span></a></li>
					</ul>

					<!-- Tabs -->
					<div class="gtco-tab-content-wrap">

						<div class="gtco-tab-content tab-content active" data-tab-content="1">
							<div class="col-md-6">
								<div class="gtco-video gtco-bg" style="background-image: url(images/img_1.jpg); ">
								</div>
							</div>
							<div class="col-md-6">
								<h2>PT Trans Media Data</h2>
								<p>
is the company	that Establish since 2018	, built	by	veteran  professional and expertise in Content Business and ICT Infrastructure Business
We have three Division to leverage our business , such as:

<li>Payment Division</li>
<li>VAS ( Valued Added	Service ) Division</li>
</p>

							</div>
						</div>

						<div class="gtco-tab-content tab-content" data-tab-content="2">
							<div class="col-md-6 col-md-push-6">
								<div class="gtco-video gtco-bg" style="background-image: url(images/img_1.jpg); ">
									<a href="https://vimeo.com/channels/staffpicks/93951774" class="popup-vimeo"></a>
									<div class="overlay"></div>
								</div>
							</div>
							<div class="col-md-6 col-md-pull-6">
								<h2>Payment Divison</h2>
								<p>TMD is a payment aggregator in Indonesia. Since inception, Redision  enables payment mode using telco vouchers, direct telco credits and also local  ATM banking for PC/Web/Mobile games Top Up. Our footprint currently covers  Indonesia, Malaysia, and in process expanding to Philippines and Thailand,  potentially serving 350 million users!

At the moment, TMD is providing services to over  110 partners with 70% being international partners.  As our business expand to various countries and  channels, our partners are using our platform to  expand to other markets to monetize their products  and services.
</p>

							</div>
						</div>

						<div class="gtco-tab-content tab-content" data-tab-content="3">
							<div class="col-md-6 col-md-push-6">
								<div class="gtco-video gtco-bg" style="background-image: url(images/content.png); ">
								</div>
							</div>
							<div class="col-md-6 col-md-pull-6">
								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Suwun</h3>
										<p>Suwun is Mobile Apps services to serve cheapest local payment online bank in indonesia</p>
									</div>
								</div> 

								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Whitelabel</h3>
										<p>Whitelabel is co-Branding Mobile Apps and Web Application services to serve cheapest local payment online bank in indonesia.</p>
									</div>
								</div>

								<div class="feature-left animate-box" data-animate-effect="fadeInLeft">
									<span class="icon">
										<i class="icon-check"></i>
									</span>
									<div class="feature-copy">
										<h3>Host to host (H2H) </h3>
										<p>Host to Host is interconect server to server services cheapest local payment online bank in indonesia.</p>
									</div>
								</div>
								
								
							</div>
						</div>

						<div class="gtco-tab-content tab-content" data-tab-content="4">
							<div class="col-md-6">
								<div class="icon icon-xlg">
									
								</div>
							</div>
							<div class="col-md-6">
								<h2>Our Experinces</h2>
								<p>TELCOS: Redpay enable gamer to use direct billing or recharge voucher from Telco:  Smartfren, Telkomsel, XL, Indosat to top up online games easily.</p>
<p>CONVENIENCE STORE: Redpay white label our payment solution to Indonesia ,  Agen POS.</p>
<p>FINANCIAL INSTITUTION: Redpay enable user to make purchases online and pay,  ATM BERSAMA,, and Visa/Master Credit card</p>

							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

	
	
	<div id="gtco-features-2">
		<div class="gtco-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center gtco-heading">
					<h2>Our Partner</h2>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/FINNET-LOGO.png); ">
					</div>
				</div>
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/logo_tapp.png); ">
					</div>
				</div>
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/indomog_logo.png); ">
					</div>
				</div>
				</div>
				<div class="row">
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/logo_redision.png); ">
					</div>
				</div>
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/unipin_logo.png); ">
					</div>
				</div>
				<div class="col-md-4">
					<div class="gtco-video gtco-video-sm gtco-bg" style="background-image: url(images/logo_sakalaguna.png); ">
					</div>
				</div>
			</div>

		</div>
	</div>

	<footer id="gtco-footer" role="contentinfo">
		<div class="gtco-container">
			<div class="row row-pb-md">
				<div class="col-md-2 col-sm-4 col-xs-6 ">
				<div class="gmap_canvas"><iframe width="437" height="426" id="gmap_canvas" src="https://maps.google.com/maps?q=Plasa%20sinar%20mas%20land%20surabaya&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
					<ul class="gtco-footer-links">
						<h3>Head Office</h3><br><h4>PT.TRANSMEDIADATA, Sinar Mas Land Plaza Lantai 12A,</h4>
						<p>  Jl. Pemuda No. 60-70, <br> Surabaya 60271 </p>
						
							<p>+(62) 031 - </p>
							<p>@transmediadata.com</p>
							<p>www.transmediadata.com</p>
					
					</ul>
					
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 ">
				<div class="gmap_canvas"><iframe width="473" height="426" id="gmap_canvas" src="https://maps.google.com/maps?q=De%20ploeit%20centrale&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div>
					<ul class="gtco-footer-links">
							<h3>Office II <h3><br><h4>PT.TRANSMEDIADATA,Gedung De PLOEIT CENTRALE,lantai 8 unit 805-806</h4>
							<P>
Jalan Pluit Selatan Raya, Pluit, Penjaringan, RT.2/RW.9, 
Pluit, Penjaringan, Kota Jkt Utara, <br> Daerah Khusus Ibukota Jakarta 14450</p>
					
							<p>+(62) 021 - 22674948</p>
							<p>@transmediadata.com</p>
							<p>www.transmediadata.com</p>
					
					</ul>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12">
					<p class="pull-left">
						<small class="block">&copy; PT.TRANSMEDIADATA. All Rights Reserved.</small> 
						<small class="block">Designed by PT.TRANSMEDIADATA </small>
					</p>
					<p class="pull-right">
						<ul class="gtco-social-icons pull-right">
							<li><a href="#"><i class="icon-instagram"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Carousel -->
	<script src="js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main.js"></script>

	</body>
</html>

